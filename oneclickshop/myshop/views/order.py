# views/order.py
""" Order is a class base view that contain:
        1: Order creation and Listing.
        2: Order retrieve, update, and delete.
"""

from django.utils.decorators import method_decorator

from rest_framework.generics import GenericAPIView

from myshop.decorators import group_required
from myshop.models import Order
from myshop.serializers import OrderSerializer
from myshop.usecases.order import (place_order, retrieve_orders,
                                   update_order, delete_order)


class OrderCreateListView(GenericAPIView):
    """ Place an order by by checking out cart
        or list all the orders.

        post:
            Place an order
    """

    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    @method_decorator(group_required('shopper'))
    def post(self, request):
        """ Create a new order
            :param request body
                {
                    "address": "address_1",
                    "city": "city_1",
                    "postal_code": "44500"
                }

            :url look like http://127.0.0.1:8000/api/orders/carts/21/

            if successfully created
            :return:
                {
                    "id": 1,
                    "address": "address_1",
                    "postal_code": "44500",
                    "city": "city_1",
                    "completed_at": "2019-05-28T11:31:04.228725Z",
                    "cart": 21
                }
            else
            :returns
                {"error": message}
            """

        return place_order(request.data, request.user.id)

    @method_decorator(group_required('admin'))
    def get(self, request):
        """ Retrieve all orders

            :param request:

            :url looks like http://127.0.0.1:8000/api/orders/carts/

            if cart found then
            :return:
                [
                    {
                        "id": 1,
                        "address": "address_1",
                        "postal_code": "44500",
                        "city": "city_1",
                        "completed_at": "2019-05-28T11:31:04.228725Z",
                        "cart": 21
                    },
                    {
                        "id": 2,
                        "address": "address_2",
                        "postal_code": "25600",
                        "city": "city_2",
                        "completed_at": "2019-05-28T11:34:09.075295Z",
                        "cart": 22
                    }
                    ....
                    ....
                ]

            else
            :returns
                {"error": {"order": ["Order history not available"]}}
        """

        return retrieve_orders()


class OrderRetrieveUpdateDeleteView(GenericAPIView):
    """ Retrieve, Update and Delete Product Entry

        get:
            retrieve a specific product entry e.g. entry_id.

        patch:
            update a specific product entry e.g entry_id.

        delete:
            delete a specific product entry e.g entry_id.
        """
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    @method_decorator(group_required('shopper'))
    def get(self, request, order_id):
        """ Retrieve a specific Order by passing order_id

            :param request:
            :param order_id

            :url looks like http://127.0.0.1:8000/api/orders/1/

            if Order found
            :return:
                {
                    "id": 1,
                    "address": "address_1",
                    "postal_code": "44500",
                    "city": "city_1",
                    "completed_at": "2019-05-28T11:31:04.228725Z",
                    "cart": 21
                }

            else
            :returns
                {"error": {"order_id": ["Order not found."]}}
        """

        return retrieve_orders(order_id)

    @method_decorator(group_required('shopper'))
    def patch(self, request, order_id):
        """ Update a specific Order by order_id
            :param request body
                {
                    "address": "address_3",
                    "city": "city_3"
                }

            :param order_id

            if successfully updated then
            :return:
                {
                    "id": 1,
                    "address": "address_3",
                    "postal_code": "44500",
                    "city": "city_3",
                    "completed_at": "2019-05-28T11:31:04.228725Z",
                    "cart": 21
                }
            else
            :returns
                {"error": message}
            """

        return update_order(request.data, order_id)

    @method_decorator(group_required('shopper'))
    def delete(self, request, order_id):
        """ Delete a specific Entry by passing entry_id.

            :param request:
            :param order_id
            :return:
                {"message": {"delete": ["deleted successfully"]}}
        """
        return delete_order(order_id)
