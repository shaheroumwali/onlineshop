# views/product.py
""" Product.py is a class base view that contains:
        1: Category creation and listing.
        2: Category retrieve, update, and delete.
        3: Product creation and listing.
        4: Product retrieve, update, and delete.
"""
from django.utils.decorators import method_decorator

from rest_framework.generics import GenericAPIView

from myshop.decorators import group_required
from myshop.models import (Product, Category)
from myshop.serializers import (CategorySerializer, ProductSerializer)
from myshop.usecases.product import (create_product, retrieve_products, update_product, delete_product,
                                     create_category, retrieve_categories, update_category,
                                     delete_category)


class CategoryCreateListView(GenericAPIView):
    """ Create a new category and return a list of all the existing categories.
        post:
            Create a new category
        get:
            :returns a list of all existing categories.
    """

    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    @method_decorator(group_required('admin'))
    def post(self, request):
        """ Create a new category.

            :url looks like http://127.0.0.1:8000/api/categories/

            :param request body
            {
                "name":"category_2",
                "stock_units":1,
            }

            if successfully created
            :return:
            {
                "id": 8,
                "name": "category_2",
                "stock_units": 1,
                "create_by": 26
            }
            else
            :returns {"error": message}
        """
        return create_category(request.post.copy(), request.user.id)

    def get(self, request):
        """ Returns a list of all the existing categories.

            :url looks like http://127.0.0.1:8000/api/categories/

            :returns a list
                [
                    {
                        "id": 7,
                        "name": "category_1",
                        "stock_units": 1,
                        "create_by": 26
                    },
                    {
                        "id": 8,
                        "name": "category_2",
                        "stock_units": 10,
                        "create_by": 27
                    },
                    {
                        "id": 9,
                        "name": "category_3",
                        "stock_units": 15,
                        "create_by": 28
                    }
                    ....
                    ....
                ]
        """
        return retrieve_categories()


class CategoryRetrieveUpdateDeleteView(GenericAPIView):
    """ Retrieve, Update and Delete Category

        get:
            retrieve a specific category by passing category_id.

        patch:
            update a specific category by passing category_id.

        delete:
            delete a specific category by passing category_id.
    """

    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get(self, request, category_id):
        """ Retrieve a specific category by passing category_id

            :param request:
            :param category_id:

            :url looks like http://127.0.0.1:8000/api/categories/10/
            :return:
                {
                    "id": 3,
                    "name": "category_3",
                    "stock_units": 1,
                    "create_by": 2
                }
        """

        return retrieve_categories(category_id)

    @method_decorator(group_required('admin'))
    def patch(self, request, category_id):
        """ Update a specific category by passing category_id.

            :param request body (change units)
                {
                    "stock_units": 213
                }

            :param category_id:

            :url looks like http://127.0.0.1:8000/api/categories/10/

            before update
                {
                    "id":10,
                    "name":"category_7",
                    "stock_units":1,
                    "create_by":26
                }

            After updating stock units
            :returns
                {
                    "id":10,
                    "name":"category_7",
                    "stock_units":213,
                    "create_by": 3
                }
        """

        return update_category(request.data, category_id)

    @method_decorator(group_required('admin'))
    def delete(self, request, category_id):
        """ Delete a specific category by passing category_id.

            :param request:
            :param category_id:
            :url looks like http://127.0.0.1:8000/api/categories/10/

            if deleted successful
            :return:
                {"message": {"delete": ["Category deleted successfully."]}}

            else
            :returns
                {"error": {"category_id": ["Category not found."]}}
        """
        return delete_category(category_id)


class ProductCreateListView(GenericAPIView):
    """ Create a new product and return a list of all the existing products.
        post:
            Create a new product
        get:
            :returns a list of all the existing products.
    """

    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    @method_decorator(group_required('admin'))
    def post(self, request):
        """ Create a new product.

            :param request body
                {
                    "name":"product_12",
                    "description": "product_12_description",
                    "stock_units":250,
                    "price": 100,
                    "category": [1,2]
                }

            if successfully created
            :return:
                {
                    "id": 23,
                    "price": 100,
                    "stock_units": 250,
                    "category": [
                        "category_1",
                        "category_4"
                    ],
                    "name": "product_12",
                    "description": "product_12_description",
                    "image": null,
                    "created_by": 4
                }
            else
            :returns
                {"error": message}
        """

        return create_product(request.data, request.user.id)

    def get(self, request):
        """ Return a List of All The Existing Products.
            :returns
               [
                    {
                        "id": 4,
                        "price": 100,
                        "name": "product_2",
                        "description": "product_2_description",
                        "image": null,
                        "is_available": true,
                        "stock_units": 0,
                        "create_by": 4,
                        "category": [
                            "category_1",
                            "category_4"
                        ],
                    },
                    {
                        "id": 15,
                        "price": 100,
                        "name": "product_5",
                        "description": "product_5_description",
                        "image": null,
                        "is_available": true,
                        "stock_units": 0,
                        "create_by": 4,
                        "category": [
                            "category_1",
                            "category_5"
                        ],
                    },
                    ....
                    ....
                ]
        """
        return retrieve_products()


class ProductRetrieveUpdateDeleteView(GenericAPIView):
    """ Retrieve, Update and Delete Product

        get:
            retrieve a specific product e.g. product_id.

        patch:
            update a specific product e.g product_id.

        delete:
            delete a specific product e.g product_id.
    """

    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    @method_decorator(group_required('shopper'))
    def get(self, request, product_id):
        """ Retrieve a specific product by passing product_id

            :param request:
            :param product_id:

            :url looks like http://127.0.0.1:8000/api/products/5/

            if product found
            :return:
               {
                    "id": 5,
                    "price": 100,
                    "stock_units": 250,
                    "category": [
                        "category_1",
                        "category_8"
                    ],
                    "name": "product_5",
                    "description": "product_5_description",
                    "image": null,
                    "created_by": 4
               }
            else if product not found
            :returns
                {"error": {"product_id": ["Product not found."]}}

        """

        return retrieve_products(product_id)

    @method_decorator(group_required('admin'))
    def patch(self, request, product_id):
        """ Update a specific product by passing product_id.

            :param request body
                {
                    "stock_units": 213
                }

            :param product_id:

            before update
               {
                    "id": 5,
                    "price": 100,
                    "stock_units": 250,
                    "category": [
                        "category_1",
                        "category_8"
                    ],
                    "name": "product_5",
                    "description": "product_5_description",
                    "image": null,
                    "created_by": 4
               }

            After update (name and stock_units updated)
            :returns
               {
                    "id": 5,
                    "price": 100,
                    "stock_units": 50,
                    "category": [
                        "category_1",
                        "category_8"
                    ],
                    "name": "updated product_5 name",
                    "description": "product_5_description",
                    "image": null,
                    "created_by": 4
               }
        """

        return update_product(request.data, product_id)

    @method_decorator(group_required('admin'))
    def delete(self, request, product_id):
        """
             Delete a specific product by passing product_id.

            :param request:
            :param product_id:

            if deleted successfully
            :return:
                {"message": {"delete": ["Product deleted successfully."]}}

            else
            :returns
                {"error": {"product_id": ["Product not found."]}}
        """

        return delete_product(product_id)
