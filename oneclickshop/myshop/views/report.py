# views/report.py
""" Report is a class base view that contain:
        1: Product Sold Report.
        2: Product Left(in Stock) Report.
"""

from django.utils.decorators import method_decorator

from rest_framework.generics import GenericAPIView

from myshop.decorators import group_required
from myshop.models import CartItem
from myshop.serializers import CartItemSerializer
from myshop.usecases.report import (sold_products, unsold_product, purchase_history)


class ProductSoldReportView(GenericAPIView):
    """ Return Summery Report of Products Sold. """

    serializer_class = CartItemSerializer
    queryset = CartItem.objects.all()

    @method_decorator(group_required('admin'))
    def get(self, request, days, hours, selected_category=None):
        """ Return summery report of products Sold in Last 12 hours, 24 hours or 7 days.

            :param request:
            :param days
            :param hours
            :param selected_category:
            :url look like: http://127.0.0.1:8000/api/reports/products/sold/days/7/hours/12/categories/1/

            if successful
            :return:
                [
                    {
                        "product__name": "product_1",
                        "quantity": 15,
                        "amount": 1500
                    },
                    {
                        "product__name": "product_1",
                        "quantity": 15,
                        "amount": 1500
                    }
                    ....
                    ....
                ]


            else:
            :returns
                {
                    "error": {
                        "time": [
                            "invalid time frame."
                        ]
                    }
                }
        """
        return sold_products(days, hours, selected_category)


class ProductLeftReportView(GenericAPIView):
    """ Returns a Summery Report of Products in Stock."""

    serializer_class = CartItemSerializer
    queryset = CartItem.objects.all()

    @method_decorator(group_required('admin'))
    def get(self, request, selected_category=None):
        """ Return a summery of all products available in Stock.

            :param request:
            :param selected_category:
            :url look like http://127.0.0.1:8000/api/reports/products/left/categories/18/
            :return:

                [
                    {
                    "name": "product_12",
                    "stock_units": 237,
                    "price": 100
                    },
                    {
                    "name": "product_17",
                    "stock_units": 250,
                    "price": 100
                    },
                    ....
                    ....
                ]
        """
        return unsold_product(selected_category)


class CustomerPurchaseHistory(GenericAPIView):
    """ Return a summary of all the products purchased by the user
    """

    serializer_class = CartItemSerializer
    queryset = CartItem.objects.all()

    @method_decorator(group_required('shopper'))
    def get(self, request):
        """ Return a summery of customer purchases .

            :param request:
            :url look like http://127.0.0.1:8000/api/reports/customer/purchase/
            :returns:
                [
                        {
                            "product__name": "product_11",
                            "quantity": 9,
                            "amount": 900
                        },
                        {
                            "product__name": "product_24",
                            "quantity": 15,
                            "amount": 1500
                        },
                        ....
                        ....
                ]

        """
        return purchase_history(request.user)
