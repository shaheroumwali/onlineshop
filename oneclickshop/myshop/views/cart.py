# views/cart.py
""" Cart is a class base view that contain:
        1: Cart creation and listing.
        2: Cart retrieve, update, and delete.
        3: Product Item retrieve, update, and delete.
"""

from django.utils.decorators import method_decorator

from rest_framework.generics import GenericAPIView

from myshop.decorators import group_required
from myshop.models import (Cart, CartItem)
from myshop.serializers import (CartSerializer, CartItemSerializer)
from myshop.usecases.cart import (add_to_cart, update_cart_item, retrieve_cart_item,
                                  delete_cart_item, create_cart, retrieve_carts, delete_cart)


class CartItemCreateListView(GenericAPIView):
    """ Add an item to the user cart and return list of items in cart.

        post:
            Add an item to the user cart
        get:
            Retrieve all items of the cart by passing cart_id
    """

    serializer_class = CartItemSerializer
    queryset = CartItem.objects.all()

    @method_decorator(group_required('shopper'))
    def post(self, request):
        """ Add aN item to cart
            :param request body
                 {
                    "product":	28,
                    "quantity": 5,
                 }

            :url look like http://127.0.0.1:8000/api/cart/item/

            if successfully created
            :return:
                {
                    "id": 89,
                    "quantity": 5,
                    "amount": "500.00",
                    "product": 28,
                    "cart": 13,
                    "customer": 42
                }
            else
            :returns
                {"error": messages}
            """

        return add_to_cart(request.data, request.user.id)

    @method_decorator(group_required('shopper'))
    def get(self, request):
        """ Retrieve all items of the cart by passing cart_id

            :param request:

            :url looks like http://127.0.0.1:8000/api/cart/item/24/

            if cart found then
            :return:
                [
                    {
                        "id": 81,
                        "quantity": 5,
                        "amount": "500.00",
                        "product": 22,
                        "cart": 13,
                        "customer": 42
                    },
                    {
                        "id": 82,
                        "quantity": 5,
                        "amount": "0.00",
                        "product": 22,
                        "cart": 13,
                        "customer": 42
                    },
                    ....
                    ....
                ]

            else
            :returns
                {"error": {"cart": ["Cart is empty yet."]}}
        """
        return retrieve_cart_item(request.user.id)


class CartItemRetrieveUpdateDeleteView(GenericAPIView):
    """ Retrieve, Update and Delete Product Entry

        get:
            retrieve a specific product entry e.g. entry_id.

        patch:
            update a specific product entry e.g entry_id.

        delete:
            delete a specific product entry e.g entry_id.
        """
    serializer_class = CartItemSerializer
    queryset = CartItem.objects.all()

    @method_decorator(group_required('shopper'))
    def get(self, request, item_id):
        """ Retrieve a specific product entry by passing entry_id

            :param request:
            :param item_id:

            :url looks like http://127.0.0.1:8000/api/carts/13/entries/88/

            if product entry found
            :return:
                {
                    "id": 88,
                    "quantity": 5,
                    "amount": "500.00",
                    "product": 27,
                    "cart": 13,
                    "customer": 42
                }

            else if product entry not found
            :returns
                {"error": {"entry_id": ["Product entry not found."]}}
        """

        return retrieve_cart_item(request.user.id, item_id)

    @method_decorator(group_required('shopper'))
    def patch(self, request, item_id):
        """ Update a specific product entry

            :param item_id
            :param request body
                {
                    "product":	27,
                    "quantity": 10
                }

            if successfully updated then
            :return:
                {
                    "id": 88,
                    "quantity": 5,
                    "amount": "500.00",
                    "product": 27,
                    "cart": 13,
                    "customer": 42
                }
            else
            :returns
                {"error": message}
            """
        return update_cart_item(request.data, item_id)

    @method_decorator(group_required('shopper'))
    def delete(self, request, item_id):
        """ Delete a specific Entry by passing entry_id.

            :param request:
            :param item_id:
            :return:
                {"message": {"delete": ["deleted successfully"]}}
        """
        return delete_cart_item(item_id)


class CartCreateView(GenericAPIView):
    """ Create a cart for customer.

        post:
            Create a cart for customer in session
    """
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

    @method_decorator(group_required('shopper'))
    def post(self, request):
        """ Create a cart  for in session user
            if successfully created
            :return:
                {
                    "id": 5,
                    "count": 0,
                    "total": "0.00",
                    "created_by": 30
                }
            else
            :returns
                {"error": messages}
        """
        return create_cart(request.user.id)


class CartRetrieveDeleteView(GenericAPIView):
    """ Retrieve, and Delete Cart

       get:
           retrieve a specific cart e.g. having cart_id.

       delete:
           delete a specific cart e.g having cart_id.
       """
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

    @method_decorator(group_required('shopper'))
    def get(self, request):
        """ Retrieve a specific cart by passing cart_id

            :param request:

            :url looks like http://127.0.0.1:8000/api/cart/retrieve/

            if cart found
            :return:
                {
                    "id": 13,
                    "customer": {
                        "id": 42,
                        "username": "umar_hayat_5",
                        "first_name": "umar",
                        "last_name": "hayat",
                        "activated": true,
                        "groups": [
                            "shopper"
                        ]
                    },
                    "total": "1800.00",
                    "checkout": false,
                    "product_names": [
                        "product_11",
                        "product_12",
                        "product_13",
                        "product_15",
                        "product_21",
                    ]
                }

            else if product not found
            :returns
                {"error": {"cart_id": ["cart not found."]}}
        """

        return retrieve_carts(request.user.id)

    @method_decorator(group_required('shopper'))
    def delete(self, request):
        """ Delete cart of active user.
            :param request:
           :url looks like http://127.0.0.1:8000/api/cart/delete/
            :return:
                {"message": {"delete": ["Cart deleted successfully"]}}
        """
        return delete_cart(request.user.id)
