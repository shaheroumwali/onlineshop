# views/registration.py
""" Registration is a class base view that contain:
        1: User registration.
        2: User account activation or email confirmation.
        3: User login.
"""
import os
from django.shortcuts import render

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic import View

from rest_framework.generics import GenericAPIView
from rest_framework.status import (HTTP_501_NOT_IMPLEMENTED)

from myshop.models import User
from myshop.serializers import (UserSerializer, UserReadSerializer)
from myshop.usecases.registration import (user_registration, send_activation_email,
                                          activate_user, user_login, user_logout,
                                          google_call_back)


class ReactAppView(View):
    """React starting view"""

    def get(self, request):
        """Get react view"""
        try:

            with open(os.path.join(settings.REACT_APP, 'index.html')) as file:
                return HttpResponse(file.read())
        except FileNotFoundError:
            return HttpResponse(
                """
                index.html not found ! build your React app !!
                """,
                status=HTTP_501_NOT_IMPLEMENTED,
            )


class UserRegistrationView(GenericAPIView):
    """ Register a new User and send activation link to user email.

        get: Send activation link to user email.
        post: Register New User and save it to database.
    """

    queryset = User.objects.all()
    serializer_class = UserSerializer

    def post(self, request):
        """ Register a new user

            :param request body
                {
                    'username':'umar_hayat_15',
                     'email': 'engr.umarhayat.login@gmail.com',
                    'first_name':'umar',
                    'last_name':'hayat',
                    'password':'abc@def.1'
                }

            if registered successfully
            :returns
                {"message": {"email": ["Please check email for account activation."]}}

            else
            :returns
            '   {'error': {message} }
        """

        activation_link = request.build_absolute_uri('/user/activate/')
        return user_registration(request.data, activation_link)

    def get(self, request):
        """ Send activation link to user email.
            :param request: body
                {
                    "username":"umar_hayat_19"
                }
            if user by username found
            :return:
                {"message": {"email": ["Please check email for account activation."]}}
            else
            :returns
                {"error": {"username": ["User Does not exist."]}}
        """

        activation_link = request.build_absolute_uri('/user/activate/')
        return send_activation_email(request.data["username"], activation_link)


class UserActivateView(GenericAPIView):
    """ Account activation and email confirmation"""

    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get(self, request, activation_id):
        """ Activate user and confirm user email through activation link.

            :param request
            :param activation_id

            if activation successful
            :returns
                {
                    "id": 40,
                    "username": "umar_hayat_3",
                    "first_name": "umar",
                    "last_name": "hayat",
                    "activated": true,
                    "groups": [
                        1
                    ]
                }

            else:
            :returns
                {'error':{'field name ': [messages]}}
        """
        return activate_user(activation_id)


class UserLoginView(GenericAPIView):
    """ User login and login token generator"""

    queryset = User.objects.all()
    serializer_class = UserReadSerializer

    def post(self, request, *args, **kwargs):
        """ Handle user login actions

            :param request:
                {
                    "username":"umar_hayat_36",
                    "password":"abc@def.1"
                }

            if successful
            :return:
                {
                    "id": 40,
                    "username": "umar_hayat_3",
                    "first_name": "umar",
                    "last_name": "hayat",
                    "activated": true,
                    "groups": [
                        1
                    ],
                    "token": "f6de2a433038f17b6d142fe13b19b0a24799d264"
                }
            else:
            :returns
                {'error': messages}

        """
        return user_login(request.data)


class UserLogoutView(GenericAPIView):
    """ Logout by deleting User auth token. """
    queryset = User.objects.all()
    serializer_class = UserReadSerializer

    @method_decorator(login_required)
    def post(self, request):
        """ Log out the already logged in user with an Authorization token

            :param request Header
                {Authorization: Token bcfc438a40cdf4f2de7d79bb5804355c7ae153c6}

            :return:
                {"message": {"session": ["successfully logged out."]}}
            """
        return user_logout(request.user)


class CallBack(GenericAPIView):
    """ Google Call back Response view
    """
    queryset = User.objects.all()
    serializer_class = UserReadSerializer

    def get(self, request):
        """
        Get auth code from google response and extract user info from the google response
        :param request:
        """
        auth_code = request.query_params.get('code', None)
        return google_call_back(auth_code)


class GoogleLogin(GenericAPIView):
    """ Google Login View test html view
    """
    queryset = User.objects.all()
    serializer_class = UserReadSerializer

    def get(self, request):
        """
            render html login view
            :param request:
            :url looks like http://127.0.0.1:8000/user/google/login/
        """
        return render(request, 'index.html')
