# myshop/models.py
""" All models for one click online shop are defined here:
    1: User: extended default user with additional activated (boolean) and activation id.
    2: Category: include name, description and stock etc.
    3: Product : includes name, description and price etc.
    4: Cart : include products, total amount, check out and time stamp.
"""

import uuid
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator
from django.db import models


class User(AbstractUser):
    """ User Model is extension of default Abstract User Model.
        Two things are added: activated (boolean) for customer email verification
        and activation id.
    """

    activated = models.BooleanField(default=False)
    activation_id = models.CharField(max_length=100, unique=True, default=uuid.uuid4)

    def __str__(self):
        return self.username


class Category(models.Model):
    """ Category data model is defined for grouping same type of Products.
        category model includes, name, stock units and time stamps.
    """

    name = models.CharField(max_length=100, unique=True)
    stock_units = models.PositiveIntegerField(default=0)
    created_by = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        """ Meta class define class behavioural attributes."""
        ordering = ('name',)
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name


class Product(models.Model):
    """ Product data model defines product attributes like name, description,
        and other fields.
    """

    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=6, decimal_places=2, validators=[MinValueValidator(0)])
    stock_units = models.PositiveIntegerField(default=1)
    image = models.ImageField(upload_to='products/%Y/%m/%d', blank=True)
    created_by = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    updated_at = models.DateTimeField(auto_now=True)
    category = models.ManyToManyField(Category, related_name='products')

    class Meta:
        """Meta class define product class behaviours"""
        ordering = ('updated_at',)
        verbose_name_plural = 'products'

    def __str__(self):
        return self.name


class Cart(models.Model):
    """ Cart data model covers total amount, products and customer details for
        in progress buying.
    """

    customer = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    product = models.ManyToManyField(Product, through='CartItem', blank=False)
    total = models.FloatField(default=0.00, null=True, blank=True, validators=[MinValueValidator(0)])
    checkout = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class CartItem(models.Model):
    """ Cart Item model define entries into the cart by adding a product into the cart
        with quantity.
    """

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)
    amount = models.FloatField(blank=True, null=True, default=0.00, validators=[MinValueValidator(0)])
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)

    class Meta:
        """ To make Product Id and User Id Unique
        """
        unique_together = ('cart', 'product',)


class Order(models.Model):
    """ Transaction data model deal with final step of shop. It includes cart id ,
        payment and customer order delivery address.
    """

    cart = models.OneToOneField(Cart, unique=True, on_delete=models.CASCADE)
    address = models.CharField(max_length=50)
    postal_code = models.CharField(max_length=10)
    city = models.CharField(max_length=20)
    completed_at = models.DateTimeField(auto_now_add=True)
