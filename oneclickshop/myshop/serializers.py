# myshop/serializers.py
""" Serializers are responsible for screening and filtering of in and out coming data
    from views of the app.

    Serializer.py includes:
    1: User read and write serializer.
    2: Category Serializer.
    3: Product Serializer.
    4: Cart Serializer.
    5: Cart Item Serializer.
    6: Order Serializer.
"""
from rest_framework import serializers
from myshop.models import (User, Product, Category, Cart, CartItem, Order)


class UserSerializer(serializers.ModelSerializer):
    """ User Serializer class is used for writing data to the database.
        It filter out all the unnecessary information from the front end if any.
    """

    class Meta:
        """ Meta class excludes while writing: is_supper,
            is_staff and user_permissions etc.
        """
        model = User
        extra_kwargs = {'password': {'write_only': True}}
        exclude = ('is_superuser', 'is_staff', 'is_active',
                   'activated', 'user_permissions', 'groups')


class UserReadSerializer(serializers.ModelSerializer):
    """ User Read Serializer class is responsible for filtering data from the database
        Model named User.
    """

    groups = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        """ Meta class define User read attributes like  username, first_name,
            last_name and activated.
         """
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'activated', 'groups')


class CategorySerializer(serializers.ModelSerializer):
    """ Category Serializer Class includes all fields of category except updated_at.
    """

    class Meta:
        """Meta class of Category Serializer"""
        model = Category
        exclude = ('updated_at',)


class ProductSerializer(serializers.ModelSerializer):
    """ Product Serializer class include all except updated_at field.
    """

    price = serializers.FloatField(min_value=0.00)
    stock_units = serializers.IntegerField(min_value=1, default=1)
    category_names = serializers.StringRelatedField(source='category', many=True, read_only=True)

    class Meta:
        """ Meta class define serializer class behavioural attributes. """
        model = Product
        extra_kwargs = {'category': {'write_only': True}}
        exclude = ('updated_at',)


class CartSerializer(serializers.ModelSerializer):
    """ Cart Serializer class is responsible for in and out data of the cart model.
        It defines product names string relation field and Product count in particular."""

    customer = UserReadSerializer(read_only=True)
    customer_id = serializers.IntegerField(write_only=True)
    total = serializers.FloatField(read_only=True, default=0.00)
    product_names = serializers.StringRelatedField(source='product', many=True, read_only=True)
    product_count = serializers.SerializerMethodField()

    class Meta:
        """ Meta class of cart serializer define cart attributes
        """
        model = Cart
        exclude = ('updated_at', 'product')

    def get_product_count(self, obj):
        """ add count parameter to cart obj
        :param obj
        """
        return obj.product.count()


class CartItemSerializer(serializers.ModelSerializer):
    """ Cart Item Serializer class is responsible for in and out data of the CartItem model.
    """

    class Meta:
        """ Meta class define Cart Item serializer class behavioural attributes """
        model = CartItem
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    """ Order Serializer class is responsible for in and out data of the Transaction model.
        It is the final stage by checking out a cart, bills payment and provide delivery address.
        """

    class Meta:
        """ Meta class define order serializer class behavioural attributes.
        """
        model = Order
        fields = '__all__'
