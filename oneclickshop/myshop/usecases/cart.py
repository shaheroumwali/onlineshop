# usercase/cart.py
""" usecase/cart.py contain create, update, retrieve, and delete related functions.
    """
from decimal import Decimal
from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST,
                                   HTTP_404_NOT_FOUND)

from myshop.models import (Product, Cart, CartItem)
from myshop.serializers import (CartSerializer, CartItemSerializer)


def update_cart_total(cart, amount):
    """ add amount to cart total and save it.
        :param cart,
        :param amount
        """
    cart.total = float(cart.total) + float(amount)
    cart.save()


def update_item_amount(item):
    """ calculate item amount.
        :param item
    """
    item.amount = item.product.price * item.quantity
    item.save()


def is_product_available(required_quantity, product_id):
    """ Check whether a product is availability in stock.
        :param required_quantity,
        :param product_id,

        if there is an error
        :returns {"error": messages}
    """

    product = Product.objects.filter(id=product_id).first()
    if product is None:
        return Response(data={"error": {"product_id": ["Product with this id does not exist."]}},
                        status=HTTP_400_BAD_REQUEST)

    if required_quantity > product.stock_units:
        return Response(data={"error": {"product": ["Product quantity is not available."]}},
                        status=HTTP_400_BAD_REQUEST)

    return Response(data={"message": "Required quantity is available in stock."},
                    status=HTTP_200_OK, )


def add_to_cart(item_dict, user_id):
    """ Add cart item(product, quantity, amount) to cart.
        :param item_dict
        :param user_id
    """

    response = is_product_available(item_dict['quantity'], item_dict['product'])
    if response.status_code is not HTTP_200_OK:  # blunder
        return response

    cart = Cart.objects.filter(customer=user_id, checkout=False).first()
    if not cart:
        cart = Cart.objects.create(customer_id=user_id)
        cart.save()

    # added cart id,and customer id
    item_dict['cart'] = cart.id
    item_dict['customer'] = user_id

    serialized_item = CartItemSerializer(data=item_dict)
    if not serialized_item.is_valid():
        return Response(data=serialized_item.errors, status=HTTP_400_BAD_REQUEST)

    item = serialized_item.save()

    update_item_amount(item)  # update cart item amount
    update_cart_total(cart, item.amount)  # add item amount in cart total
    return Response(data=serialized_item.data, status=HTTP_201_CREATED)


def update_cart_item(item_dict, item_id):
    """ Update the cart item by providing updated data and item_id.
        :param item_dict
        :param item_id
    """

    pre_updated_item = CartItem.objects.filter(id=item_id, cart__checkout=False).first()
    if not pre_updated_item:
        return Response(data={"error": {"item_id": ["Item with this id does not exist."]}},
                        status=HTTP_404_NOT_FOUND)

    cart = Cart.objects.filter(id=pre_updated_item.cart_id, checkout=False).first()
    if not cart:
        return Response(data={"error": {"cart_id": ["Cart with this id does not exist."]}})

    response = is_product_available(item_dict['quantity'], pre_updated_item.product.id)
    if response.status_code is not HTTP_200_OK:
        return response

    serialized_item = CartItemSerializer(pre_updated_item, data=item_dict, partial=True)
    if not serialized_item.is_valid():
        return Response(data=serialized_item.errors, status=HTTP_400_BAD_REQUEST)
    updated_item = serialized_item.save()

    update_cart_total(cart, -pre_updated_item.amount)  # restore cart total to previous state
    update_item_amount(updated_item)  # update cart item amount
    update_cart_total(cart, updated_item.amount)  # call to update cart total function

    return Response(data=serialized_item.data, status=HTTP_201_CREATED)


def retrieve_cart_item(user_id, item_id=None):
    """
        Return a cart item by item id or all existing items into a cart.
        :param item_id:
        :param user_id
    """
    if item_id is None:
        items = CartItem.objects.filter(cart__customer=user_id, cart__checkout=False).all()
    else:
        items = CartItem.objects.filter(id=item_id, cart__checkout=False).all()

    if not items:
        return Response(data={"message": {"item_id": ["Item in cart does not exists."]}},
                        status=HTTP_404_NOT_FOUND)

    serialized_entries = CartItemSerializer(items, many=True)
    return Response(data=serialized_entries.data, status=HTTP_200_OK)


def delete_cart_item(item_id):
    """
        Remove a product item from cart by passing item id
        :param item_id:
    """
    item = CartItem.objects.filter(id=item_id, cart__checkout=False).first()

    if item is None:
        return Response(data={"error": {"item": ["Item with this id does not exist."]}},
                        status=HTTP_404_NOT_FOUND)

    cart = Cart.objects.filter(id=item.cart_id).first()
    cart.total -= item.amount
    cart.save()

    item.delete()
    return Response(data={"message": {"delete": ["Item deleted successfully"]}})


def create_cart(user_id):
    """
        Create user cart by passing user id
        :param user_id:
    """
    cart = Cart.objects.filter(customer=user_id, checkout=False).first()
    if cart is not None:
        serialized_cart = CartSerializer(cart)
        return Response(data=serialized_cart.data, status=HTTP_200_OK)

    cart_dict = {"customer_id": user_id}  # Creating new cart if no previous unchecked cart exists
    serialized_cart = CartSerializer(data=cart_dict)
    if not serialized_cart.is_valid():
        return Response(data=serialized_cart.errors, status=HTTP_400_BAD_REQUEST)
    serialized_cart.save()

    return Response(data=serialized_cart.data, status=HTTP_201_CREATED)


def retrieve_carts(user_id):
    """
        Return a user cart by passing user id
        :param user_id
    """

    carts = Cart.objects.filter(customer=user_id, checkout=False).first()
    if not carts:
        return Response(data={"error": {"user_id": ["This User's unchecked cart does not exist."]}},
                        status=HTTP_404_NOT_FOUND)
    serialized_carts = CartSerializer(carts)
    return Response(data=serialized_carts.data, status=HTTP_200_OK)


def delete_cart(user_id):
    """
        Delete user cart by passing user id
        :param user_id:
    """
    cart = Cart.objects.filter(customer=user_id, checkout=False).first()
    if cart is None:
        return Response(data={"error": {"user_id": ["This User's unchecked cart does not exist."]}},
                        status=HTTP_404_NOT_FOUND)
    cart.delete()
    return Response(data={"message": {"delete": ["Cart deleted successfully"]}})
