# usecase/Order.py
""" order.py contain the order details related functions like
    create order, update Order or update order .
"""

from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST,
                                   HTTP_404_NOT_FOUND)

from myshop.models import Order, Cart, CartItem
from myshop.serializers import OrderSerializer
from myshop.usecases.product import update_product_quantity


def place_order(order_dict, user_id):
    """
        Create new order by checking out a cart payment
        :param order_dict:
        :param user_id:
    """
    cart = Cart.objects.filter(customer_id=user_id, checkout=False).first()
    if not cart:
        return Response(data={"message": {"order": ["No item in Cart yet."]}},
                        status=HTTP_404_NOT_FOUND)

    order_dict["cart"] = cart.id

    serialized_order = OrderSerializer(data=order_dict)
    if not serialized_order.is_valid():
        return Response(data=serialized_order.errors, status=HTTP_400_BAD_REQUEST)

    serialized_order.save()

    cart.checkout = True
    cart.save()

    items = CartItem.objects.filter(cart__id=cart.id).all()
    for item in items:
        update_product_quantity(-item.quantity, item.product)

    return Response(data=serialized_order.data, status=HTTP_201_CREATED)


def retrieve_orders(order_id=None):
    """
        Return order by order id or all existing orders.
    """
    if order_id is None:
        orders = Order.objects.all()
    else:
        orders = Order.objects.filter(id=order_id).all()

    serialized_orders = OrderSerializer(orders, many=True)
    return Response(data=serialized_orders.data, status=HTTP_200_OK)


def update_order(order_dict, order_id):
    """
        Update a order by order id
        :param order_id:
        :param order_dict:

    """
    order = Order.objects.filter(id=order_id, cart__checkout=False).first()

    if order is None:
        return Response(data={"error": {"order_id": ["Order with this id does not exist."]}},
                        status=HTTP_404_NOT_FOUND)

    serialized_order = OrderSerializer(Order, data=order_dict, partial=True)
    if not serialized_order.is_valid():
        return Response(data=serialized_order.errors, status=HTTP_400_BAD_REQUEST)

    serialized_order.save()

    return Response(data=serialized_order.data, status=HTTP_201_CREATED)


def delete_order(order_id):
    """
        Delete a order by passing order id
        :param order_id
    """
    order = Order.objects.filter(id=order_id).first()
    if order is None:
        return Response(data={"error": {"order_id": ["Order with this id does not exist."]}},
                        status=HTTP_404_NOT_FOUND)

    order.cart.checkout = False
    order.cart.save()

    items = CartItem.objects.filter(cart__id=order.cart.id).all()
    for item in items:
        update_product_quantity(item.quantity, item.product)
    order.delete()
    return Response(data={"message": {"delete": ["deleted successfully"]}}, status=HTTP_200_OK)
