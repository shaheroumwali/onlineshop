# usecases/cart.py
""" product.py contains:
        1: add new product function
        2: update product function
        3: delete category function
        4: all above function for category too.
    """
from myshop.models import Product, Category
from myshop.serializers import ProductSerializer, CategorySerializer
from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST,
                                   HTTP_404_NOT_FOUND)


def create_category(category_dict, user_id):
    """
        Add a new category to the database
        :param category_dict:
        :param user_id:
    """
    category_dict['created_by'] = user_id

    serialized_category = CategorySerializer(data=category_dict)

    if not serialized_category.is_valid():
        return Response(data={"error": serialized_category.errors}, status=HTTP_400_BAD_REQUEST)

    serialized_category.save()
    return Response(data=serialized_category.data, status=HTTP_201_CREATED)


def retrieve_categories(category_id=None):
    """
        Return category by passing category id

        :param category_id:

        if category_id is None
        Then :returns all categories
        else :returns Category having category_id as id.
    """
    if category_id is None:
        categories = Category.objects.all().order_by('name')
    else:
        categories = Category.objects.filter(id=category_id).all()
        if not categories:
            return Response(data={"error": {"category_id": ["Category with this id does not exist."]}},
                            status=HTTP_404_NOT_FOUND)

    serialized_categories = CategorySerializer(categories, many=True)
    return Response(data=serialized_categories.data, status=HTTP_200_OK)


def update_category(category_dict, category_id):
    """
        Update an already existing category by passing category id
        :param category_dict:
        :param category_id:
    """

    category = Category.objects.filter(id=category_id).first()

    if category is None:
        return Response(data={"error": {"category_id": ["Category with this id does not exist."]}},
                        status=HTTP_404_NOT_FOUND)

    serialized_category = CategorySerializer(category, data=category_dict, partial=True)
    if not serialized_category.is_valid():
        return Response(data=serialized_category.errors, status=HTTP_400_BAD_REQUEST)
    serialized_category.save()

    return Response(data=serialized_category.data, status=HTTP_200_OK)


def delete_category(category_id):
    """
        Delete a category from the database.
        :param category_id:
    """
    category = Category.objects.filter(id=category_id).first()

    if category is None:
        return Response(data={"error": {"category_id": ["Category with this id does not exist."]}},
                        status=HTTP_404_NOT_FOUND)

    category.delete()
    return Response(data={"message": {"delete": ["Category deleted successfully."]}})


def update_product_quantity(quantity, product):
    """
        Update product quantity in stock
        :param quantity:
        :param product:
        :return: {"message": {"stock_units": ["Product quantity updated successfully."]}}
    """
    product.stock_units += quantity
    product.save()
    return Response(data={"message": {"quantity": ["Product quantity updated successfully."]}})


def create_product(product_dict, user_id):
    """
        Add a new product to the database
        :param product_dict:
        :param user_id:
    """
    # print("check@01: ", type(product_dict))
    product_dict['created_by'] = user_id  # it bugs her "This QueryDict instance is immutable"

    serialized_product = ProductSerializer(data=product_dict)
    if not serialized_product.is_valid():
        return Response(data={"error": serialized_product.errors}, status=HTTP_400_BAD_REQUEST)
    serialized_product.save()

    return Response(data=serialized_product.data, status=HTTP_201_CREATED)


def retrieve_products(product_id=None):
    """
        Return product by passing product id
        :param product_id:

        if product_id is None
        Then :returns all products
        else :returns product having product_id as id.
    """
    if product_id is None:
        products = Product.objects.all().order_by('-price')
    else:
        products = Product.objects.filter(id=product_id).all()
        if not products:
            return Response(data={"error": {"product_id": ["Product with this id does not exist."]}},
                            status=HTTP_404_NOT_FOUND)

    serialized_products = ProductSerializer(products, many=True)
    return Response(data=serialized_products.data, status=HTTP_200_OK)


def update_product(product_dict, product_id):
    """
        Update already existing product
        :param product_dict:
        :param product_id:

        if successfully created
        :return:
            {
                "id": 23,
                "price": 100,
                "stock_units": 250,
                "category": [
                    "category_1",
                    "category_4"
                ],
                "name": "product_12",
                "description": "product_12_description",
                "image": null,
                "created_by": 4
            }
        else
        :returns
            {"error": message}

    """

    product = Product.objects.filter(id=product_id).first()

    if product is None:
        return Response(data={"error": {"product_id": ["Product with this id does not exist."]}},
                        status=HTTP_404_NOT_FOUND)

    serialized_product = ProductSerializer(product, data=product_dict, partial=True)
    if not serialized_product.is_valid():
        return Response(data=serialized_product.errors, status=HTTP_400_BAD_REQUEST)
    serialized_product.save()

    return Response(data=serialized_product.data, status=HTTP_200_OK)


def delete_product(product_id):
    """
        Delete an existing product by passing product id
        :param product_id:

    """
    product = Product.objects.filter(id=product_id).first()

    if product is None:
        return Response(data={"error": {"product_id": ["Product with this id does not exist."]}},
                        status=HTTP_404_NOT_FOUND)
    product.delete()

    return Response(data={"message": {"delete": ["Product deleted successfully."]}})
