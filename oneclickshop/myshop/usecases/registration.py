# usecase/registration.py
""" order.py contain the registration related functions like
    create transaction, update transaction or update transaction .
"""
from oauth2client import client

from django.conf import settings
from django.contrib.auth.models import Group, update_last_login
from django.utils import timezone

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST)

from myshop.models import User
from myshop.serializers import (UserSerializer, UserReadSerializer)


def user_registration(request_data, activation_link):
    """
        Register a new user by passing user data
        :param request_data:
        :param activation_link:
    """
    request_data['username'] = request_data['email']
    serialized_user = UserSerializer(data=request_data)
    if not serialized_user.is_valid():
        return Response(data={"error": serialized_user.errors}, status=HTTP_400_BAD_REQUEST)

    user = serialized_user.create(serialized_user.validated_data)

    user.set_password(user.password)
    user.clean()
    user.save()

    return send_activation_email(user.username, activation_link)


def send_activation_email(user_name, activation_link):
    """
        Send activation email y passing user name and activation redirect link
        :param user_name:
        :param activation_link:
    """
    try:
        user = User.objects.filter(username=user_name).first()
        if user.activated:
            return Response(data={"error": 'User is already activated.'},
                            status=HTTP_400_BAD_REQUEST)

        subject = 'One Click Online Shop Activation Link'
        body = ("Hi ," + user.get_full_name() + " Hope you are doing fine. "
                                                "To activate your account,"
                + " please click on the link below: \n"
                + activation_link + str(user.activation_id))
        user.email_user(subject, body)
        return Response(data={"message": {"email": ["Please check email for "
                                                    "account activation."]}},
                        status=HTTP_201_CREATED)
    except AttributeError:
        return Response(data={"error": {"username": ["User Does not exist."]}},
                        status=HTTP_400_BAD_REQUEST)


def activate_user(activation_id):
    """
        Activate a user by passing user activation id
        :param activation_id:
    """
    user = User.objects.filter(activation_id=activation_id).first()

    if user is None:
        return Response(data={"error": 'Invalid activation id'}, status=HTTP_400_BAD_REQUEST)

    if user.activated:
        return Response(data={"error": 'User is already activated.'},
                        status=HTTP_400_BAD_REQUEST)

    customer_group = Group.objects.get(name='shopper')
    user.groups.add(customer_group)
    user.activated = True
    user.save()

    serialized_user = UserReadSerializer(user)
    return Response(data=serialized_user.data, status=HTTP_200_OK)


def user_login(request_data):
    """
        User login takes teh credential of the user and generate a token for the user.
        :param request_data:
        :return: user token
    """
    request_data['username'] = request_data['email']
    serialized_user = ObtainAuthToken.serializer_class(data=request_data)

    if not serialized_user.is_valid(raise_exception=True):
        return Response(data={'error': serialized_user.errors}, status=HTTP_400_BAD_REQUEST)

    user = serialized_user.validated_data['user']

    if not user.activated:
        return Response(data={'error': {'email': ['Please confirm your email first.']}},
                        status=HTTP_400_BAD_REQUEST)

    update_last_login(sender=None, user=user)
    user.save()

    token, created = Token.objects.get_or_create(user=user)
    serialized_user_data = UserReadSerializer(user).data

    serialized_user_data['token'] = token.key
    return Response(data=serialized_user_data, status=HTTP_200_OK)


def user_logout(user):
    """
        Logout user by delete user auth token
        :param user:
    """
    user.auth_token.delete()
    return Response(data={"message": {"session": ["User successfully logged out."]}},
                    status=HTTP_200_OK)


def google_call_back(auth_code):
    """
        extract user information from the Google JSON response
        :param auth_code:
    """
    if auth_code is None:
        return Response("auth code not found.")

    scopes = ['https://www.googleapis.com/auth/userinfo.email']
    credentials = client.credentials_from_clientsecrets_and_code(
        filename=settings.CLIENT_SECRET_FILE, scope=scopes, code=auth_code,
        redirect_uri='http://localhost:8000/user/google/callback/')

    email = credentials.id_token['email']
    username = email
    user = User.objects.filter(username=username).first()
    user.last_login = timezone.now()
    user.save()
    token, created = Token.objects.get_or_create(user=user)
    serialized_user_data = UserReadSerializer(user).data

    serialized_user_data['token'] = token.key
    return Response(data=serialized_user_data, status=HTTP_200_OK)
