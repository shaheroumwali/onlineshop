# usecase/cart.py # usecase
""" report.py contain the sold product and update product functions.
    It takes the required time frame, and selected category to return a full list of
    products sold or left in the stocks.
"""
from django.utils import timezone

from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK)

from myshop.models import (Product, CartItem, Order)


def sold_products(days=None, hours=None, selected_category=None):
    """
        Return a list of sold product based on selected category

        :param days
        :param hours
        :param selected_category:
    """

    time_frame = timezone.now() - timezone.timedelta(days=days, hours=hours)

    if selected_category is None:
        cart_id = Order.objects.filter(completed_at__gte=time_frame,
                                       cart__checkout=True).values_list('cart', flat=True)
        product_sold_report = CartItem.objects.filter(cart__in=cart_id, ). \
            values('product__name', 'quantity', 'amount')
    else:
        cart_id = Order.objects.filter(completed_at__gte=time_frame,
                                       cart__checkout=True).values_list('cart', flat=True)

        product_sold_report = CartItem.objects.filter(cart__in=cart_id,
                                                      product__category=selected_category). \
            values('product__name', 'quantity', 'amount')

    return Response(data=product_sold_report, status=HTTP_200_OK)


def unsold_product(selected_category):
    """
        Return a list of unsold product in a selected category
        :param selected_category:

    """
    if selected_category is None:
        product_left_report = Product.objects.all().values('name', 'stock_units', 'price')
    else:
        product_left_report = Product.objects.filter(category=selected_category). \
            values('name', 'stock_units', 'price')
    return Response(data=product_left_report, status=HTTP_200_OK)


def purchase_history(user):
    """
        Return all time purchase history of a given user
        :param user:
    """
    cart_id = Order.objects.filter(cart__customer=user, cart__checkout=True). \
        values_list('cart', flat=True).all()

    customer_purchase_report = CartItem.objects.filter(cart__in=cart_id, ). \
        values('product__name', 'quantity', 'amount')
    return Response(data=customer_purchase_report, status=HTTP_200_OK)
