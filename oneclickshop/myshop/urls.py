# myshop/urls.py
""" my shop urls includes:
        1: Category create list view url
        2: Product create list view url
        3: Registration, login, and logout url
        4: Email confirmation url
"""

from django.urls import path
from myshop.views.registration import (UserRegistrationView, UserActivateView, CallBack,
                                       GoogleLogin)
from myshop.views.product import (CategoryCreateListView, CategoryRetrieveUpdateDeleteView,
                                  ProductCreateListView, ProductRetrieveUpdateDeleteView)
from myshop.views.cart import (CartCreateView, CartRetrieveDeleteView, CartItemCreateListView,
                               CartItemRetrieveUpdateDeleteView)
from myshop.views.report import (ProductSoldReportView, ProductLeftReportView,
                                 CustomerPurchaseHistory)
from myshop.views.order import (OrderCreateListView, OrderRetrieveUpdateDeleteView)

URLS = [
    # URL for creation and listing of category
    path(r'api/categories/', CategoryCreateListView.as_view(), name='api-category-create-list'),

    # URL for update, delete and retrieve of category
    path(r'api/categories/<int:category_id>/', CategoryRetrieveUpdateDeleteView.as_view(),
         name='api-category-retrieve-update-delete'),

    # URL for creation and listing of product
    path(r'api/products/', ProductCreateListView.as_view(), name='api-product-create-list'),

    # URL for update, delete and retrieve of product
    path(r'api/products/<int:product_id>/', ProductRetrieveUpdateDeleteView.as_view(),
         name='api-product-retrieve-update-delete'),

    # URL for creating a new cart
    path(r'api/cart/', CartCreateView.as_view(), name='api-cart-create'),

    # URL for deletion
    path(r'api/cart/delete/', CartRetrieveDeleteView.as_view(), name='api-carts-delete'),

    # URL for cart retrieving
    path(r'api/cart/retrieve/', CartRetrieveDeleteView.as_view(), name='api-carts-retrieve'),

    # URL for add new item into a cart and list all items of user unchecked cart
    path(r'api/cart/items/', CartItemCreateListView.as_view(), name='api-cart-item-create-list'),

    # URL for update, delete, and retrieve of a product entry
    path(r'api/cart/items/<int:item_id>/',
         CartItemRetrieveUpdateDeleteView.as_view(), name='api-cart-item-retrieve-update-delete'),

    # URL for order creation and List (only for admin) all orders
    path(r'api/orders/', OrderCreateListView.as_view(),
         name='api-order-create-list'),

    # URL for update, delete and retrieve of cart
    path(r'api/orders/<int:order_id>/',
         OrderRetrieveUpdateDeleteView.as_view(), name='api-order-retrieve-update-delete'),

    # URL for reporting  products sold
    path(r'api/reports/products/sold/days/<int:days>/hours/<int:hours>/categories/',
         ProductSoldReportView.as_view(), name='api-Products-sold-without-category-selection'),

    # URL for reporting  products sold of a specific category
    path(r'api/reports/products/sold/days/<int:days>/hours/<int:hours>/categories/<int:selected_category>/',
         ProductSoldReportView.as_view(), name='api-Products-sold-with-category-selection'),

    # URL for reporting products left in stock
    path(r'api/reports/products/left/categories/', ProductLeftReportView.as_view(),
         name='api-Products-left-with-category-selection'),

    # URL for reporting product left in stock of specific category
    path(r'api/reports/products/left/categories/<int:selected_category>/',
         ProductLeftReportView.as_view(), name='api-Products-left-without-category-selection'),

    # URL for reporting customer purchase history
    path(r'api/reports/customer/purchase/', CustomerPurchaseHistory.as_view(),
         name='api-customer-purchase-history'),

    # URL for User Registration
    path(r'user/registration/', UserRegistrationView.as_view(), name='api-user-registration'),

    # URL for sending activation email
    path(r'user/activate/<uuid:activation_id>/', UserActivateView.as_view()),

    # URL for using Google sign in option
    path(r'user/google/login/', GoogleLogin.as_view(), name='api-google-login'),

    # URL for google callback data receiving
    path(r'user/google/callback/', CallBack.as_view(), name='api-google-call-back'),

]
