# myshop/decorator.py
""" Decorator for checking if the group has the required access or not.
"""
from django.contrib.auth.decorators import user_passes_test
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED


def group_required(*group_names):
    """Requires user membership in at least one of the groups passed in."""

    def in_groups(user):

        if user.groups.filter(name__in=group_names) or user.is_superuser:
            return True
        else:
            return Response(data={"message": "You do not have permission to access this."},
                            status=HTTP_401_UNAUTHORIZED)

    return user_passes_test(in_groups,)
