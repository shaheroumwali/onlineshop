from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework.status import (HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST)
from myshop.models import User, Category, Product, CartItem, Cart


# Create your tests here.
class WebTestCases(TestCase):
    def setUp(self):
        """
        SetUp: creates a test user, test category,
        test product, test brands and test cart."""

        # creating test base user
        user = User.objects.create_user(username="test_user_5", email="user@test.com")
        user.set_password("test@test.5")
        user.is_active = True
        user.activated = True
        user.is_superuser = True
        user.last_login = timezone.now()
        user.save()

        # user credential and Client API setup
        self.client = APIClient()
        self.auth_login_api = reverse(viewname='api-user-Login')
        self.test_user_credential = {"username": "test_user_5", "password": "test@test.5"}

        # Getting test user
        self.test_user = User.objects.get(username="test_user_5")

        # Test Category
        self.test_category = Category.objects.create(created_by=self.test_user, name="test_category")
        self.test_category.stock_units = 10
        self.test_category.save()

        # Test Product 1
        self.test_product_1 = Product.objects.create(created_by=self.test_user, price=10, name="test_product_1")
        self.test_product_1.stock_units = 10
        self.test_product_1.category.set([self.test_category.id])
        self.test_product_1.save()
        # Test Product 1
        self.test_product_2 = Product.objects.create(created_by=self.test_user, price=15, name="test_product_2")
        self.test_product_2.stock_units = 10
        self.test_product_2.category.set([self.test_category.id])
        self.test_product_2.save()

        # Test Cart
        self.test_cart = Cart.objects.create(customer=self.test_user)
        self.test_cart.save()

        # Test CartItem
        self.test_cart_item = CartItem.objects.create(product=self.test_product_1,
                                                  cart=self.test_cart, amount=100)
        self.test_cart_item.quantity = 10
        self.test_cart_item.save()

    def testCategoryCreateListView(self):
        """ Test: Category Creation and Listing."""

        category_url = reverse(viewname='api-category-create-list')

        def test_positive(self):
            """Test: positive Category"""

            category = {"name": "category_12", "stock_units": 10}
            self.client.force_authenticate(user=self.test_user)

            response = self.client.post(path=category_url, data=category, format='json')
            self.assertEquals(response.status_code, HTTP_201_CREATED)

            category = {"name": "Electronics"}
            response = self.client.post(path=category_url, data=category, format='json')
            self.assertEquals(response.status_code, HTTP_201_CREATED)

        def test_negative(self):
            """Test: negative Category"""

            # invalid name blank
            category = {"name": None, "stock_units": 10}
            self.client.force_authenticate(user=self.test_user)

            response = self.client.post(path=category_url, data=category, format='json')
            self.assertEquals(response.status_code, HTTP_400_BAD_REQUEST)

            # invalid created_by
            category = {"name": "Electronics", "created_by": "waleed"}
            response = self.client.post(path=category_url, data=category, format='json')
            self.assertEquals(response.status_code, HTTP_400_BAD_REQUEST)

        def test_get():
            response = self.client.get(path=category_url, format='json')
            self.assertEquals(response.status_code, HTTP_200_OK)

        test_positive(self)  # call to test positive function
        test_negative(self)  # call to negative function
        test_get()  # call to get all category function

    def testProductCreateListView(self):
        """ Test adding a new Product and Listing all products. """

        product_url = reverse(viewname='api-product-create-list')
        self.client.force_authenticate(user=self.test_user)

        def test_positive(self):
            """ Test adding a new product (positive)"""

            # Valid data all arguments provided
            product = {"name": "test_product_3", "description": "test_product_3_description",
                       "stock_units": 250, "price": 100, "category": [self.test_category.id]}

            response = self.client.post(path=product_url, data=product, format='json')
            self.assertEquals(response.status_code, HTTP_201_CREATED)

            # Valid : just values changed
            product = {"name": "test_product_4", "description": "test_product_4_description",
                       "stock_units": 3, "price": 10, "category": [self.test_category.id]}
            response = self.client.post(path=product_url, data=product, format='json')
            self.assertEquals(response.status_code, HTTP_201_CREATED)

        def test_negative(self):
            """ Test adding a new product (negative)"""

            # InValid: Name Field None
            product = {"name": None, "description": "test_product_description",
                       "stock_units": 250, "price": 100, "category": [self.test_category.id]}

            response = self.client.post(path=product_url, data=product, format='json')
            self.assertEquals(response.status_code, HTTP_400_BAD_REQUEST)

            # InValid : Stock units in negative
            product = {"name": "test_product_1", "description": "test_product_description",
                       "stock_units": -3, "price": 10, "category": [self.test_category.id]}
            response = self.client.post(path=product_url, data=product, format='json')
            self.assertEquals(response.status_code, HTTP_400_BAD_REQUEST)

            # InValid : Price in negative
            product = {"name": "test_product_1", "description": "test_product_description",
                       "stock_units": 3, "price": -10, "category": [self.test_category.id]}
            response = self.client.post(path=product_url, data=product, format='json')
            self.assertEquals(response.status_code, HTTP_400_BAD_REQUEST)

            # InValid : Category Not provided
            product = {"name": "test_product_1", "description": "test_product_description",
                       "stock_units": 3, "price": -10, "category": [None]}
            response = self.client.post(path=product_url, data=product, format='json')
            self.assertEquals(response.status_code, HTTP_400_BAD_REQUEST)

        def test_get_list(self):
            """ Test Getting List of all products"""
            response = self.client.get(path=product_url, format='json')
            self.assertEquals(response.status_code, HTTP_200_OK)

        test_positive(self)  # call to test positive function
        test_negative(self)  # call to negative function
        test_get_list(self)  # Call GetList function

    def testCartItemCreateView(self):
        """ Test adding a new Product  item and listing all items into a cart. """

        add_to_cart_url = reverse(viewname='api-cart-item-create-list')
        self.client.force_authenticate(user=self.test_user)

        def test_positive(self):
            """ Test adding a new product to the cart (positive)"""

            # Valid data all arguments provided
            item = {"product": self.test_product_2.id, "quantity": 9}

            response = self.client.post(path=add_to_cart_url, data=item, format='json')
            self.assertEquals(response.status_code, HTTP_201_CREATED)

        def test_negative(self):
            """ Test adding a new product to the cart (negative)"""

            # InValid: quantity not provided
            entry = {"product": self.test_product_2.id, "quantity": -3}

            response = self.client.post(path=add_to_cart_url, data=entry, format='json')
            self.assertEquals(response.status_code, HTTP_400_BAD_REQUEST)

            # InValid: product id not exist
            entry = {"product": None, "quantity": 9}
            response = self.client.post(path=add_to_cart_url, data=entry, format='json')
            self.assertEquals(response.status_code, HTTP_400_BAD_REQUEST)

        test_positive(self)  # call to test positive function
        test_negative(self)  # call to negative function

    def tearDown(self):
        self.test_product_1.delete(using=None, keep_parents=False)
        self.test_product_2.delete(using=None, keep_parents=False)
        self.test_category.delete(using=None, keep_parents=False)
