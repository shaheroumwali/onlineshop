"""oneclickshop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path


from rest_framework_swagger.views import get_swagger_view

from oneclickshop.settings import ADMIN_ENABLED
from myshop.urls import URLS
from myshop.views.registration import (ReactAppView, UserLoginView, UserLogoutView)


SCHEMA_VIEW = get_swagger_view(title='One Click Shop')

urlpatterns = [

    # URL for login
    path('user/login/', UserLoginView.as_view(), name='api-user-Login'),

    # URL for logout
    path('user/logout/', UserLogoutView.as_view(), name='api-user-Logout'),

    # URL for document of all backend API's
    url(r'^docs/', SCHEMA_VIEW),

    # URL for interaction
    url(r'^$', ReactAppView.as_view()),
    url(r'^onclick', ReactAppView.as_view()),

]

if ADMIN_ENABLED:
    urlpatterns += [path('admin/', admin.site.urls)]

urlpatterns += URLS
