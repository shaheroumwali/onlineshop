**One Click Online Shop Backend Code:**

**Introduction:**

This app is an online micro e-commerce platform where users can come in and buy stuff. Category are defined for each type of different products.

The app has two basic components:

*  Backend Server
*  Web Client 
 

**Backend Server:**
This project is in the root /oneclickshop and oneclickshop folders in the project root.

Instructions to Setup and run Dev Environment:

**Requirements:**

* Project requires python 3.6 or newer.
* It is recommended to use a separate environment for the project. You can use conda or vitrualenv.



**Steps to run Backend Server:**

* Clone Git repo.
* This project uses postgres as database.
* Make sure postgres is running on (default) port 5432 (If you want to run on a non default port you can change it from oneclickshop -> settings.py)
* Set database username and password in oneclickshop -> settings.py
* 
* To setup a new database launch psql on terminal and run following command CREATE DATABASE one_click_shop_app
* 
* Run the migration scripts ONE_CLICK_SHOP_APP_DB_PASSWORD=$DBPASSWORD python3 manage.py migrate
* (Replace $DBPASSWORD with actual DB password)
* Launch the server ONE_CLICK_SHOP_APP_DB_PASSWORD=$DBPASSWORD python3 manage.py runserver
* (Replace $DBPASSWORD with actual DB password)
* Open the API docs http://127.0.0.1:8000/docs/
